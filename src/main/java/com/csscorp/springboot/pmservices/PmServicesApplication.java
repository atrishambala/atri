package com.csscorp.springboot.pmservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PmServicesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PmServicesApplication.class, args);
	}
}
